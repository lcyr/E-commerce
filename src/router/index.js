import { createRouter, createWebHistory } from "vue-router"
import cookie from "js-cookie"
import Layout from '../layout/main.vue'
import { userCommonStore } from "../store/common.js";
import { clearLoginInfo } from "../util/index.js";


const globalRouters = [
    {
        path: '/',
        component: () => import(),
        name: '404',
        meta: { title: '404未找到'}
    },
    {
      path: 'login',
      component: () => import('../views/common/login/login.vue'),
      name: '登录',
      meta: { title: '登录'}
    }
]
export const mainRouters = {
    path: '/',
    component:Layout,
    name: 'home',
    redirect: '/home',
    children: [
        {
            path: 'home',
            name: 'home',
            component: () => import('../views/common/home/index.vue')
        },
        {
            path: '/prodInfo',
            name: 'prodInfo',
            component: () => import('../views/modules/prod/prodInfo/index.vue')
        }
    ],
    beforeEnter(to,from,next) {
        const authorization = cookie.get('Authorization')
        if (!authorization ||!/^S/.test(authorization)) {
            clearLoginInfo()
            next({ name:'login' })
        }
        next()
    }
}
const router = createRouter({
    history: createWebHistory(),
    scrollBehavior: () => ({ top: 0}),
    isAddDynamicMenuRoutes: false, //是否添加到动态路由
    routes: globalRouters.concat(mainRouters)
})
// 添加动态(菜单)路由
// 1. 已经添加 或者 全局路由, 直接访问
// 2. 获取菜单列表, 添加并保存本地存储
router.beforeEach((to,from,next) => {
    const commonStore = userCommonStore()
    if (router.options.isAddDynmaicMenuRoutes || fnCurrentRouteType(to,globalRouters) === 'global') {
        const routeList = commonStore.routeList
        let navTitles = []
        let leftMenuId = ''
        routeList.forEach(item => {
            if (to.meta.menuId === item.menuId) {
                navTitles.push(item.name)
                routeList.forEach(item1 => {
                    if (item.parentId=== item1.menuId){
                        navTitles.push(item1.name)
                        leftMenuId = item.parentId
                        routeList.forEach(item2 => {
                            if (item1.parentId === item2.menuId) {
                                navTitles.push(item2.name)
                                leftMenuId = item1.parentId
                            }
                        })
                    }
                })
            }
        })
        navTitles = navTitles.reverse()
        if (to.meta.isLeftMenu || to.path === '/home' || leftMenuId) {
            if (leftMenuId) {
                commonStore.updateSelectLeftId(leftMenuId)
                commonStore.updateSelectLeftId(to.path === '/home' ?'': to.meta.menuId)
            }
        }
        commonStore.updateSelectMenu(navTitles)
        next()
    }else {
        http({
            url:http.adornUrl('/sys/menu/nav'),
            method: 'get',
            params: http.adornParams()
        }).then(({ data }) => {
            sessionStorage.setItem('Authorities',JSON.stringify(data.authorities ||'[]'))
            fnAddDynamicMenuRoutes(data.menuList)
            router.options.isAddDynmaicMenuRoutes = true
            const rList =[]
            data.menuList.forEach(item => {
                item.isLeftMenu = item.parentId === 0
                rList.push({
                    menuId: item.menuId,
                    name: item.name,
                    parentId: item.parentId,
                    url: item.url
                })
                if (item.list) {
                    item.list.forEach(item1 => {
                        item1.isLeftMenu = item1.parentId === 0
                        rList.push({
                            menuId: item1.menuId,
                            name: item1.name,
                            parentId: item1.parentId,
                            url: item1.url
                        })
                        if (item1.list) {
                            item.list.forEach(item2 => {
                                item1.isLeftMenu = item2.parentId === 0
                                rList.push({
                                    menuId: item2.menuId,
                                    name: item2.name,
                                    parentId: item2.parentId,
                                    url: item2.url
                                })
                            })
                        }
                    })
                }
            })
                fnAddDynamicMenuRoutes(data,menuList)
                sessionStorage.setItem('menuList',JSON.stringify(data.menuList || '[]'))
                commonStore.updateRouteList(rList)
                commonStore.updateMenuIds(rList)
                next({ ...to,replace:true })
        }).catch(error => {
            console.log(`%c${error} 请求菜单列表和权限失败,跳转至登录页面!!`,`color:blue`)
            router.push({ name : 'login'})
        })
    }
})
/**
 * 判断当前路由类型
 * @param route 当前路由
 * @param globalRoutes 全局路由
 */
function fnCurrentRouteType(route,globalRoutes = []) {
    let temp = []
    for (let i = 0; i < globalRoutes; i++) {
        if (route.path === globalRoutes[i].path) {
            return 'gloabl'
        } else if(globalRoutes[i].children && globalRoutes[i].children.length >= 1) {
           temp =  temp.concat(globalRoutes[i].children)
        }
    }
    return temp.length >=1 ? fnCurrentRouteType(route,globalRoutes) : 'main'
}

/**
 * 添加动态(菜单)路由
 * @param menuList 菜单列表
 * @param routes 递归创建动态(菜单)路由
 */
function fnAddDynamicMenuRoutes(menuList = [],routes =[]) {
    let temp = []
    const modules = import.meta.glob('../views/modules/**/index.vue')
    for (let i = 0; i < menuList.length; i++) {
         if (menuList[i].list && menuList[i].list.length >=1) {
            temp =  temp.concat(menuList[i].list)
         } else if (menuList[i].url && /\^S/.test(menuList[i].url)) {
               menuList[i].url = menuList[i].replace(/^V/,'')
             const route = {
                  path: menuList[i].url,
                  component: null,
                  name:menuList[i].url,
                 meta: {
                     menuId: menuList[i].menuId,
                     title: menuList[i].name,
                     isDynamic:true,
                     iframeUrl:''
                 }
             }
             if (isURL(menuList[i].url)) {
                 route.path = `i-${menuList[i].menuId}`
                 route.name  = `i-${menuList[i].menuId}`
                 route.meta.iframeUrl = menuList[i].url
             } else {
                 try {
                     route.component = modules[`../views/modules/${menuList[i].url}/index.vue`] || null
                 }catch (e){}
             }
             routes.push(route)
         }
    }
    if (temp.length >= 1) {
        fnAddDynamicMenuRoutes(temp,routes)
    } else {
        mainRouters.name = 'main-dynamic'
        mainRouters.children = routes
        router.addRoute(mainRouters)
    }
    router.addRoute({ path: '/:pathMatch(.*)*',redirect: { name:'404'} })
}
export default router