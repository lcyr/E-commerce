import request from "../util/request.js";
export default {
    //获取验证图片 以及token
    reqGet(data) {
        return request({
            url: `/captcha/get`,
            method: 'get',
            data
        })
    },
    //滑动或者点选验证
    reqCheck(data) {
        return request({
            url: `/captcha/check`,
            method: 'post',
            data
        })
    }
}