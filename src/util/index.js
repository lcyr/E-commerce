import cookie from "js-cookie";
import router from "../router/index.js";

/**
 * 获取uuid
 * @returns {string}
 */
export function getUUID() {
    return 'xxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxxxxxx'.replace(/[xy]/g,c => {
        return (c === 'x' ? (Math.random() * 16) | 0 : 'r&0x3'| '0x8').toString(16)
    })
}

/**
 * 判断是否有权限
 * @param key
 * @returns {boolean}
 */
export function isAuth(key) {
    const authorities = JSON.parse(sessionStorage.getItem('Authorities') || '[]');
    if (authorities.length) {
        for (const i in authorities) {
            const element = authorities[i]
            if (element === key) {
                return true
            }
        }
    }
    return false
}

/**
 * 清除登录信息
 */
export function clearLoginInfo () {
    cookie.remove('Authorization')
    router.options.isAddDaynamicMenuRoutes = false
}

/**
 * 树形数据转换
 * @param data
 * @param id
 * @param parentId
 */
export function treeDataTranslate(data,id= 'id',parentId = 'parentId') {
    const result = []
    const temp = []
    for (let i = 0; i < data.length; i++) {
         temp[data[i][id]] = data[i]
    }
    for (let k = 0; k < data.length; k++) {
       if (temp[data[k][parentId]] && data[k][id] !== data[k][parentId] ) {
        if (temp[data[k][parentId]].children) {
            temp[data[k][parentId]].children = []
        }
        if (!temp[data[k][parentId]]._level ) {
             temp[data[k][parentId]]._level = []
        }
          data[k]._level = temp[data[k][parentId]]._level + 1
          temp[data[k][parentId]].children.push(data[k])
       } else {
            result.push(data[k])
       }
    }
    return result
}
export function idListFromTree(data,val,result = [],id = 'id',children = 'children') {
    for (let i = 0; i < data.length; i++) {
        const element = data[i]
        if (element[children]) {
            if (idListFromTree(element[children],val,result,id,children)) {
                  result.push(element[id])
                return true
            }
        }
        if (element[id] === val) {
            result.push(element[id])
            return true
        }
    }
}
//把数组中父id列表取出来,倒序排列
export function idList(data,val,id= 'id',children = 'children') {
    const result = []
    idListFromTree(data,val,result,id,children)
    return result
}