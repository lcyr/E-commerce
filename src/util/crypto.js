import CryptoJS from 'crypto-js'
const keyStr = '-mall4j-password'
export function encrypto(word){
    const time = Date.now()
    const key = CryptoJS.enc.Utf8.parse(keyStr)
    const srcs = CryptoJS.enc.Utf8.parse(time+ word)
    const encrypted = CryptoJS.AES.encrypt(srcs,key,{
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    })
    return encrypted.toString()
}