import axios from 'axios'
axios.default.baseURL = import.meta.env.VITE_APP_BASE_API
const service =axios.create({
    timeout: 4000,
    headers: {
        'X-Requested-With' :'XMLHttpRequest',
        'Content-Type': 'application/json;charset=UTF-8'
    }
})

service.interceptors.request.use(
    config => {
        return config
    },
    error => {
        Promise.reject(error)
    }
)
service.interceptors.response.use(
    response => {
        return response.data
    }
)
export default service