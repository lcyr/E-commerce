import { createApp,defineAsyncComponent } from 'vue'
import  { createPinia} from "pinia";
import moment from 'moment'
import 'element-plus/dist/index.css'
import * as ElementPlusIconVue from '@element-plus/icons-vue'
import locale from 'element-plus/lib/locale/lang/zh-cn.js'
import Avue from '@smallwei/avue'
import '@smallwei/avue/lib/index.css'
import '@/style/index.scss'
import  'virtual:svg-icons-register'
import svgIcon from '@/icons/SvgIcon.vue'
moment.locale('zh-cn',{
    longDateFormat: {
        LT: 'HH:mm',
        LTS: 'HH:mm:ss',
        L: 'YYYY-MM-DD',
        LL: 'YYYY-MM-DD HH:mm:ss'
    },
    week: {
        dow:1, // 星期一
        doy: 4 // 1月4日
    }
})
import App from './App.vue'
import ElementPlus from 'element-plus'
import "element-plus/dist/index.css"
import Router from "./router/index.js";
// const AsyncButton = defineAsyncComponent(() => import('./components/HelloWorld.vue'))
const app = createApp(App)
app.component('SvgIcon',svgIcon)
app.use(Avue)
// app.use('AsyncButton',AsyncButton)
app.use(ElementPlus,{ locale })
for (const [key,component] of Object.entries(ElementPlusIconVue)) {
    app.component(key,component)
}
app.use(Router)
app.use(createPinia)
app.mount('#app')
// createApp(App).mount('#app')
