import {defineStore} from "pinia"
import router from "../router/index.js";
export const userCommonStore = defineStore("common",{
    state: () => {
        return {
            documentClintHeight: 0, //页面文档可视高度
            sidebarLayoutSkin: 'dark', // 侧边栏, 布局皮肤, light(浅色) / dark(黑色)
            sidebarFold: true, // 侧边栏, 折叠状态
            menuList: [],  // 侧边栏, 菜单
            menuActiveName: '',
            mainTabs: [], // 主入口标签页
            mainTabsActiveName: '',
            selectMenu: [], // 当前选择的标签
            routeList: [], //路由列表
            menuIds: [],
            selectLefId: '',
            selectRightId:''
        }
    },
    actions : {
        updateDocumentClientHeight(height){
             this.documentClintHeight = height
        },
        updateSidebarLayoutSkin(skin){
            this.sidebarLayoutSkin = skin
        },
        updateSidebarFold(fold){
            this.sidebarFold = fold
        },
        updateMenuList(list){
            this.menuList = list
        },
        updateMenuActiveName(name){
            this.menuActiveName = name
        },
        updateMainTabs(tab){
            this.mainTabs = tab
        },
        updateMainTabsActiveName(name){
            this.mainTabsActiveName = name
        },
        updateSelectMenu(menu){
            this.selectMenu = menu
        },
        updateRouteList(list){
            this.routeList = list
        },
        updateMenuIds(list){
            this.menuIds = []
            list.forEach(menu => {
                this.menuIds.push(String(menu.menuIds +''))
            })
        },
        removeMainActiveTab() {
            this.mainTabs = this.mainTabs.filter(item => item.name !== this.mainTabsActiveName)
            if (this.mainTabs.length >=1 ){
                //当被tab选中就删除
                router.push({ name: this.mainTabs[this.mainTabs.length -1].name},() => {
                    this.mainTabsActiveName = this.mainTabs[this.mainTabs.length -1].name
                })
            } else {
                this.menuActiveName =''
                router.push({ name: 'home'})
            }
        },
        updateSelectLefId(id){
            this.selectLefId = id
        },
        updateSelectRightId(id){
            this.selectRightId = id
        },
    }
})