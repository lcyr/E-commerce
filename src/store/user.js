import { defineStore } from "pinia";
export const useUseStore = defineStore('user',{
    state: () => {
        return {
            id: 0,
            name: '',
            userId: '',
            shopId: '',
            mobile: ''
        }
    },
    actions: {
        updateId(id) {
            this.id = id
        },
        updateName(name){
            this.name = name
        },
        updateUserId(userId) {
            this.userId = userId
        },
        updateShopId(shopId) {
            this.shopId = shopId
        },
        updateMobile(mobile) {
            this.mobile = mobile
        }
    }
})