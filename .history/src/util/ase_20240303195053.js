import CryptoJS from 'crypto-js'
/**
 * @param {*} word 加密的内容
 * @param {*} keyWord service随机返回的关键字
 * @returns 
 */
export function aesEncrypt(word,keyWord='XwKsGlMcdPMEhR1B') {
  const key = CryptoJS.enc.Utf8.parse(keyWord)
  const srcs = CryptoJS.enc.Utf8.parse(word)
  const encrypted = CryptoJS.AEC.encrypt(srcs,key,{ mode: CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7})
  return encrypted.toString()
}