import { defineConfig,loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from "path";
// import Volar from 'volar/vite'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import { createSvgIconsPlugin } from "vite-plugin-svg-icons";
import viteCompression from 'vite-plugin-compression'
import eslintPlugin from "vite-plugin-eslint";
import ElementPlus from 'unplugin-element-plus/vite'
// https://vitejs.dev/config/
export default defineConfig(() => {
 return {
  plugins: [
      vue(),
    // Volar(),
      createSvgIconsPlugin({
        iconDirs: [path.resolve(process.cwd(),'src/icons/svg')],
        symbolId: 'icon-[dir]-[name]'
      }),
      // 自动引入内容
      AutoImport({
        imports: ['vue','vue-router'],
        dirs: ['src/hooks/**','src/stores/**','src/utils/**'],
        resolvers: [ElementPlusResolver()],
        dts: 'src/auto-import/imports.d.ts',
        eslintrc: {
          enabled: false
        }
      }),
      //自动引入组件
      Components({
        dirs: ['src/components'],
        resolvers: [ElementPlusResolver()],
        dts: 'src/auto-import/components.d.ts'
      }),
      eslintPlugin({
         include: ['src/**/*.js', 'src/**/*.vue', 'src/*.js', 'src/*.vue']
      }),
      //大于1k的文件进行压缩
      viteCompression({
         threshold: 1000
      })
  ],
  server: {
    port: 80,
    host: true,
    open: true
  },
  resolve: {
     alias: {
      '@': path.resolve(__dirname,'src'),
      'vue-i18n': 'vue-i18n/dist/vue-i18n.cjs.js'
    }
  },
  build: {
    base: './',
    rollupOptions: {
       //静态资源打包
      output: {
        chunkFileNames: 'static/js/[name]-[hash].js',
        entryFileNames: 'static/js/[name]-[hash].js',
        assetsFileNames: 'static/[ext]/[name]-[hash].[ext]',
        // 静态资源分拆打包
        manualChunks (id) {
          if (id.includes('node_modules')) {
            if (id.toString().indexOf('.pnpm/') !== -1) {
              return id.toString().split('.pnpm/')[1].split('/')[0].toString();
            } else if (id.toString().indexOf('node_modules/') !== -1) {
              return id.toString().split('node_modules/')[1].split('/')[0].toString();
            }
          }
        }
      }
    },
    sourcemap: false,
    target: 'es2015',
    reportCompressedSize: false
  }
 }
})
